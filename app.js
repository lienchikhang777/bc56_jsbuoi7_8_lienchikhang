var array = [];
var array_2 = [];
const RESULT_1 = '#result_1';
const RESULT_2 = '#result_2';
const RESULT_3 = '#result_3';
const RESULT_4 = '#result_4';
const RESULT_5 = '#result_5';
const RESULT_6 = '#result_6';
const RESULT_7 = '#result_7';
const RESULT_8 = '#result_8';
const RESULT_9 = '#result_9';
const RESULT_10 = '#result_10';
const ARRAY_1 = '#array';
const ARRAY_2 = '#array_2'
/**common function */
function addNumber(type) {
    switch (type) {
        case 'array':
            handleAddNumber(array, '#number', ARRAY_1);
            break;
        default:
            handleAddNumber(array_2, '#number_2', ARRAY_2);
            break;
    }
}

function handleAddNumber(arr, selector, arrLocation) {
    var number = document.querySelector(selector).value * 1;
    arr.push(number);
    document.querySelector(selector).value = '';
    renderArray(arrLocation, arr);
}

function renderArray(selector, arr) {
    document.querySelector(`${selector}`).innerHTML = arr;
}


function renderResult(str, result, selector) {
    document.querySelector(`${selector}`).innerHTML = `
        <p>${str}: ${result}</p>
    `
}
/**end-common */

/**cau 1 vs 2 common */
function handlePositiveNumber() {
    var sum = 0;
    var count = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            sum += array[i];
            count++;
        }
    }
    return { sum, count };
}

/**cau 1 */
function getSumOfPositiveNumbers() {
    var result = handlePositiveNumber().sum;
    var str = "Sum"
    renderResult(str, result, RESULT_1);
}

/**cau 2 */
function getCountOfPositiveNumber() {
    var result = handlePositiveNumber().count;
    var str = "Count"
    renderResult(str, result, RESULT_2);
}

/**cau 3 vs 4 common */
function isPositive(n) {
    if (n > 0) {
        return true;
    }
    return false;
}

function getPositiveArray() {
    return newArr = array.filter(function (arr) {
        return arr > 0;
    })
}


function findPositiveMin() {
    var newArr = getPositiveArray();

    var min = newArr[0];
    for (var i = 1; i < newArr.length; i++) {
        if (newArr[i] < min) {
            min = newArr[i]
        }
    }
    return min;
}

function findMin() {
    var min = array[0];
    for (var i = 1; i < array.length; i++) {
        if (array[i] < min) {
            min = array[i];
        }
    }
    return min;
}

function handleFindMin(type) {
    switch (type) {
        case 'positive':
            return findPositiveMin();
        default:
            return findMin();
    }
}

/**cau 3 */
function findMinimumNumber() {
    var result = handleFindMin('');
    var str = 'min: '
    renderResult(str, result, RESULT_3);
}

/**cau 4 */
function findMinimumPositiveNumber() {
    var result = handleFindMin('positive');
    var str = 'positive min: '
    renderResult(str, result, RESULT_4);
}

/**cau 5 */
function getEvenArray() {
    return newArr = array.filter(function (arr) {
        return arr % 2 == 0
    })
}
function getLastOfEvenNumber() {
    var newArr = getEvenArray();
    console.log("🚀 ~ file: app.js:114 ~ getLastOfEvenNumber ~ newArr:", newArr)
    var result = 0;
    if (newArr == '') {
        result = -1;
    } else {
        result = newArr[newArr.length - 1];
    }
    var str = "the last of even num"
    renderResult(str, result, RESULT_5);
}

/**cau 6 */
function swappingByIndex() {
    var newArr = [];
    for (var i = 0; i < array.length; i++) {
        newArr.push(array[i]);
    }

    var firstNum = document.querySelector('#firstNumber').value * 1;
    var secondNum = document.querySelector('#secondNumber').value * 1;


    if (firstNum < 0 || firstNum >= array.length || secondNum < 0 || secondNum >= array.length) {
        renderResult('Không tìm thấy vị trí', `${firstNum} - ${secondNum}`, RESULT_6)
    } else {
        var temp = newArr[firstNum];
        newArr.splice(firstNum, 1, newArr[secondNum]);
        newArr.splice(secondNum, 1, temp);

        console.log(newArr)

        renderArray(RESULT_6, newArr);
    }
}

/**cau 7 */
function getSortArray() {
    var newArr = [];
    for (var i = 0; i < array.length; i++) {
        newArr[i] = array[i];
    }

    newArr.sort(function (a, b) {
        return a - b;
    });
    renderArray(RESULT_7, newArr);
}

/**cau 8 */
function handleGetFirstPrimeNum() {
    for (var i = 0; i < array.length; i++) {
        if (array[i] > 1) {
            if (array[i] == 2) {
                return 2;
            }
            if (array[i] == 3) {
                return 3;
            }
            if (array[i] % 2 == 0)
                continue;
            else {
                if (array[i] % 3 == 0)
                    continue;
                else
                    return array[i];
            }
        }
    }
}

function getFirstPrimeNum() {
    var str = "Prime Number"
    var result = handleGetFirstPrimeNum();
    renderResult(str, result, RESULT_8);
}


/**cau 9 */
function countIntegerNumber() {
    var str = "Count";
    var result = handleCountIntegerNumber();
    renderResult(str, result, RESULT_9);
}

function handleCountIntegerNumber() {
    var count = 0;
    for (var i = 0; i < array_2.length; i++) {
        if (!Number.isInteger(array_2[i]))
            continue;
        else
            count++;
    }
    return count;
}

/**cau 10 */
function compareOddAndEven() {
    var str = "";
    var sumOfPosNum = getPositiveArray().length; // function nam o cau 3 VS 4 COMMON
    var sumOfNegNum = getNegativeArray().length;
    if (sumOfPosNum > sumOfNegNum)
        str = "DƯƠNG > ÂM";
    else if (sumOfPosNum < sumOfNegNum)
        str = "ÂM > DƯƠNG "
    else
        str = "DƯƠNG = ÂM"
    renderResult("Result", str, RESULT_10);
}

function getNegativeArray() {
    return array.filter(function (arr) {
        return arr < 0
    })
}
